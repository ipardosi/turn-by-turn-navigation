/*
 * Copyright (c) 2011-2019 HERE Europe B.V.
 * All rights reserved.
 */

import UIKit
import NMAKit

// To obtain the application credentials, please register at https://developer.here.com/develop/mobile-sdks
let credentials = (
    appId: "omRkJkYtbL2rRsQSNfyq",
    appCode: "IY2FkcmO4rd5YNygxZrTyQ",
    licenseKey: "T2kgwdXuz2Kjhk5qE8Vmt5l9SIhE43lKrHQ1JmIrL7FsZ66JCmahBjfAGY/8wMzJNzJW4M3QYNgcUvfW5x6e20LErU7Vbnc7NPYq4eFBnlL+8q4nsgWYmVGs6L1rJChgDEU/lISO0tJTSaoKsUyob+IsUhn7xNp0u+BL0UMIFvYt5UuYSHrlLcSNhTcZi+yNxq9zUgZJei9cs/S3EEq8AKM/lP5BvO6PvSvEgjqXB/E/gtPZF71iY9SUGlCJp6YpSPU5TgtuphVrDF65P8RAMQffNrAFIch1ZPaeow4DC5Koa9BdjGGkx+MkA2+iX3w9u/hw+aUxr6Wn/elhnUcJe13fUjC4qx+zZop0IJoBQd6Xb941Nm80GbNoMqyY1PW5+bcrO0h5Ej5iBmYKjSZ08LBhPBNdmoZLsACrdIYjnxfxouBZl+TKrbSR5nFByIDTFOgw8KivHEFdIoiLK24CE93PIXIViKzHLAWws9tEKbagp7RQZ6it2DXPlOb4whcMpJ+F/vt/Byo8yw6ZwbxJEy10uArv6OuqKE2BN3I+IAKeF6nOimZFk+BFp/zc4YPDpdjcCHM5tsPbxjELk1rxATsYUM9oP7lcgVJvA54HWdOv+hMoLWTf36Beky2eIGt/U65XCkRD5d8t7ZAlCxLdoHi3ISpgS6/Nyw/6Olf3bHc="
)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        NMAApplicationContext.setAppId(credentials.appId, appCode: credentials.appCode, licenseKey: credentials.licenseKey)
        return true
    }
}
